function Animal() {
    this.raca = "shi tzu";
    this.nome = "yoda";
    this.idade = 4;
    this.peso = 15;

    //crair uma ação
    this.fazerXixi = function () {
        console.log("................");
    }
    this.comer = function (kg) {
        console.log("comendo.....");
        this.peso = this.peso + kg;
    }
}

var yoda = new Animal();

var beth = new Animal();
beth.raca = "gato";
beth.nome = "beth";