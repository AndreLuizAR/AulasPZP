//definir propriedades
function Animal() {
    this.raca = "shi tzu";
    this.nome = "yoda";
    this.idade = "4 anos";
    this.peso = "15Kg";
}

//instanciar um objeto, transforma em um objeto real
var yoda = new Animal();

var beth = new Animal();
beth.raca = "gato";
beth.nome = "beth";