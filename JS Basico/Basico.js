//<script src="Basico.js"></script>

//receber dado atraves de um caixa de dialogo
var nome = prompt('Qual seu nome?');

//abrir caixa de dialogo
alert("Olá " + nome +  "!");

//escrever algo na tela
document.write('Isso foi escrito pelo JS');

//Ir para o google
function Google() {
    window.open('http://google.com.br', '_blank');
}

//Mudar Texto
function Mudar() {
    document.getElementById('mudar').innerHTML= 'Mudado';
}

//Função para a lampada com Botao
function Ligar() {
    document.getElementById('Lampada').src = "img/Ligar.jpg";
}
function Desligar() {
    document.getElementById('Lampada').src = "img/Desligar.jpg";
}

//Adicionar elemento em uma lista e Limpar ela
function Adicionar() {
    var Adicionar = document.getElementById('Adicionar').value;
    var Lista = document.getElementById('Lista').innerHTML;

    Lista = Lista + "<li>" + Adicionar + "</li>";

    document.getElementById('Lista').innerHTML = Lista;
}
function Limpar() {
    var Lista = document.getElementById('Lista').innerHTML;

    Lista = "<br>";
    document.getElementById('Lista').innerHTML=Lista;
}

//Operações matematicas com recebimento de valor
function CalcularTrapezio(){
    var b = parseFloat(document.getElementById('BaseMenor').value);
    var B = parseFloat(document.getElementById('BaseMaior').value);
    var h = parseFloat(document.getElementById('H').value);
    var A = ((b+B)/2)*h;
    document.getElementById('ATra').innerHTML = A;
}

//Sortear um numero
function Sorteio() {
    var sortear = Math.floor(Math.random() * 100);
    document.getElementById('sorteio').innerHTML = sortear;
}