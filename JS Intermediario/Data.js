function SaberHora() {
    var tempo = new Date();
    document.getElementById("hora").innerHTML =
        tempo.getHours() + " horas e " + tempo.getMinutes() + " minutos";
}

//com switch
function SaberData() {
    var tempo = new Date();
    var mes = tempo.getMonth();

    switch (mes) {
        case 0:
            var mes = "Janeiro";
            break;
        case 1:
            var mes = "Fevereiro";
            break;
        case 2:
            var mes = "Março";
            break;
        case 3:
            var mes = "Abril";
            break;
        case 4:
            var mes = "Maio";
            break;
        case 5:
            var mes = "Junho";
            break;
        case 6:
            var mes = "Julho";
            break;
        case 7:
            var mes = "Agosto";
            break;
        case 8:
            var mes = "Setembro";
            break;
        case 9:
            var mes = "Outubro";
            break;
        case 10:
            var mes = "Novembro";
            break;
        case 11:
            var mes = "Dexembro";
            break;
    }

    document.getElementById("data").innerHTML =
        "Hoje é dia " +tempo.getDate()+ " de " +mes+ " de " + tempo.getFullYear();
}

//com array
function SaberSemana() {
    var tempo = new Date();
    var semana = ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sabado"];
    document.getElementById("semana").innerHTML = semana[tempo.getDay()];

}